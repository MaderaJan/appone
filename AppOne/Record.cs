﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppOne
{
    class Record
    {
        public string UUID { get; set; }
        public String Title { get; set; }
        public String Text { get; set; }
        public String Pritority { get; set; }
        public String Type { get; set; }
        public String TypeImage { get; set; }
        public DateTime Date { get; set; }

        public Record(string uuid, String title, String text, DateTime date)
        {
            UUID = uuid;
            Title = title;
            Text = text;
            Date = date;
        }
    }
}
